// @ts-check
// Note: type annotations allow type checking and IDEs autocompletion

const lightCodeTheme = require('prism-react-renderer/themes/github');
const darkCodeTheme = require('prism-react-renderer/themes/dracula');

const DefaultLocale = 'en';

/** @type {import('@docusaurus/types').Config} */
const config = {
  title: 'Kazarma - Documentation',
  tagline: 'Kazarma docs',
  url: 'https://docs.kazar.ma',
  baseUrl: '/',
  onBrokenLinks: 'throw',
  onBrokenMarkdownLinks: 'warn',
  favicon: 'img/favicon.ico',

  // GitHub pages deployment config.
  // If you aren't using GitHub pages, you don't need these.
  // organizationName: 'facebook', // Usually your GitHub org/user name.
  // projectName: 'docusaurus', // Usually your repo name.

  // Even if you don't use internalization, you can use this field to set useful
  // metadata like html lang. For example, if your site is Chinese, you may want
  // to replace "en" with "zh-Hans".
  i18n: {
    defaultLocale: 'en',
    locales: ['en', 'fr', 'es'],
  },

  presets: [
    [
      'classic',
      /** @type {import('@docusaurus/preset-classic').Options} */
      ({
        docs: {
          sidebarPath: require.resolve('./sidebars.js'),
          // Please change this to your repo.
          // Remove this to remove the "edit this page" links.
          routeBasePath: '/', // Serve the docs at the site's root
          editUrl: ({locale, versionDocsDirPath, docPath}) => {
            if (locale !== DefaultLocale) {
              return `https://crowdin.com/project/kazarma_docs/${locale}`;
            }
            
            return `https://gitlab.com/technostructures/kazarma/kazarma_docs/-/blob/main/${versionDocsDirPath}/${docPath}`;
          }
        },
        blog: false,
        // blog: {
        //   showReadingTime: true,
        //   // Please change this to your repo.
        //   // Remove this to remove the "edit this page" links.
        //   editUrl:
        //     'https://github.com/facebook/docusaurus/tree/main/packages/create-docusaurus/templates/shared/',
        // },
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
      }),
    ],
  ],

  themeConfig:
    /** @type {import('@docusaurus/preset-classic').ThemeConfig} */
    ({
      navbar: {
        title: 'Kazarma - Documentation',
        // logo: {
        //   alt: 'My Site Logo',
        //   src: 'img/logo.svg',
        // },
        items: [
          // {
          //   type: 'doc',
          //   docId: 'user-doc',
          //   position: 'left',
          //   label: 'User documentation',
          // },
          // {
          //   type: 'doc',
          //   docId: 'admin-doc',
          //   position: 'left',
          //   label: 'Admin documentation',
          // },
          // {to: '/blog', label: 'Blog', position: 'left'},
          {
            href: 'https://gitlab.com/technostructures/kazarma/kazarma',
            label: 'GitLab',
            position: 'right',
          },
          {
            type: 'localeDropdown',
            position: 'right',
          },
        ],
      },
      footer: {
        style: 'dark',
        links: [
          {
            title: 'Links',
            items: [
              {
                label: 'Source code',
                href: 'https://gitlab.com/technostructures/kazarma/kazarma',
              },
              {
                label: 'Issue tracker (feature requests, bug reports...)',
                href: 'https://gitlab.com/technostructures/kazarma/kazarma/-/issues',
              },
              {
                label: 'Matrix',
                href: 'https://matrix.to/#/#kazarma:matrix.org',
              },
            ],
          },
          {
            title: 'Sponsors',
            items: [
              {
                label: 'NGI Zero by NLnet',
                href: 'https://nlnet.nl/NGI0/',
              },
            ],
          },
        ],
        copyright: `Built with Docusaurus.`,
      },
      prism: {
        theme: lightCodeTheme,
        darkTheme: darkCodeTheme,
      },
    }),
};

module.exports = config;
