---
sidebar_position: 5
---

# Chat (Pleroma)

Kazarma is compatible with the chat system in Pleroma.

- To create a conversation, first [get the address for the recipient "puppet"](search).
- From Pleroma, you can simply enter their address in the "Create chat" search field.
- From Matrix, you can click the "+" button next to "People" in the sidebar.

:::caution

Those messages are *not* end-to-end encrypted.

:::
