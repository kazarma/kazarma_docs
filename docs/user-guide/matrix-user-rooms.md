---
sidebar_position: 3
---

# Matrix user rooms

You can create a room where all messages that you create are forwarded as public Notes.

- Create a public room
- Invite the Kazarma bot (for the public instance, it's [`@kazarma:kazar.ma`](https://matrix.to/#/@kazarma:kazar.ma))
- Send `!kazarma outbox` to the room

Messages sent by other Matrix users in the room will create public posts mentioning you.

You can send `!kazarma outbox off` to turn off the bridging.

Follow requests by ActivityPub users are automatically accepted.
