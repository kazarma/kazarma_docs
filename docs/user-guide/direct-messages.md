---
sidebar_position: 4
---

# Direct messages

Direct messages, as they exist in Mastodon and Pleroma, are a way to exchange private messages between microblogging users, by using the "private" (or "direct") visibility of posts (the posts are sent only to the mentioned users).

From Mastodon and Pleroma, you can choose this visibility setting before sending a new post.

From Matrix, you need to invite the "Matrix puppet" (whose address can be found using [search](search)), in a private room with disabled encryption.

:::caution

Those messages are *not* end-to-end encrypted.

:::
