---
sidebar_position: 2
---

# ActivityPub user rooms

You can join Matrix rooms that show all public activities created by an ActivityPub actor (user, group, channel…).

For any given actor whose Matrix puppet is `@user___server:homserver`, their outbox room will have an alias `#user___server:homeserver` (the `@` is changed to a `#`). This alias is shown when [searching for an actor](search#search-for-an-activitypub-user).

These rooms are created for both users and groups (PeerTube channels for instance).

If the room does not exist, searching for its alias will trigger its creation.

## Forwarded activities

Those type of activities are forwarded to outbox rooms:

- Post (public posts in Mastodon and Pleroma)
- Video (from PeerTube)
- Event (from Mobilizon)

## Replies

You can reply to those transferred events to send a public comment.

## Sending a message

Sending a message in an ActivityPub outbox room will create a public post mentioning the actor.

## Follow

Send `!kazarma follow` in the room to follow the user, and `!kazarma unfollow` to unfollow.
