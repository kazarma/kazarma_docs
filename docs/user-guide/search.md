---
sidebar_position: 1
---

# Search

You can use Kazarma's Web interface to search for Matrix and ActivityPub users. This quickly gives you the address to reach them from the other network.

To use the public instance, navigate to [https://kazar.ma](https://kazar.ma).

## Search for a Matrix user

To search for a Matrix user, enter their Matrix username, in the form `@user:server`.

## Search for an ActivityPub user

To search for an ActivityPub user, you can use two types of address:
- their ActivityPub username, in the form `user@server`;
- their ActivityPub ID, which is often the URL of their profile page (`https://server/@user`).
