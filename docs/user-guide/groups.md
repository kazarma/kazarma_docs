---
sidebar_position: 6
---

# Groups

You can invite a Matrix user to a Mobilizon group. This will create a private room and invite the user. If they join the room, they will accept the group invitation.

Internal discussions will be forwarded to the room.

:::info

Your Mobilizon instance and the Kazarma instance you want to use will need to federate. Ask your instance administrator.

:::
