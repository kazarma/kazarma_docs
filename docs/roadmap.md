---
sidebar_position: 2
---

# Roadmap

Here, Matrix puppet refers to a "fake" Matrix user, handled by Kazarma, representing an ActivityPub user. Similarly, AP puppet refers to a "fake" ActivityPub user, handled by Kazarma, representing a Matrix user.

- [x] User search
  - [x] Matrix users handled by a Kazarma instance appear when looked up by an AP server as AP puppets that are created if they don't exist
  - [x] AP users appear when looked up by a Matrix user as Matrix puppets that are created if they don't exist
- [x] User profile (display name and avatar)
  - [x] AP puppet profile is set upon creation
  - [x] AP puppet profile is changed when matrix user profile changes and the change is federated to AP network
  - [x] Matrix puppet profile is set upon creation
  - [x] Matrix puppet profile is changed when AP user profile changes
- [x] Web interface
  - [x] Users can search for Matrix or AP users using their Matrix ID, AP username or AP ID
  - [x] The interface provides information on how to reach those users using their respective puppets on the other network
  - [x] The interface shows last public activities for actors
- [x] Chat messages (Pleroma only)
  - [x] Sending a message to a Matrix direct room with a Matrix puppet relays it as a Pleroma direct message to the corresponding AP user
  - [x] Sending a message to an AP puppet using Pleroma direct message interface makes the corresponding Matrix puppet send a message to its direct conversation with the corresponding Matrix user (and creates the room if it doesn't exist)
- [x] Private posts / direct messages (Pleroma & Mastodon)
  - [x] Sending a message to a Matrix private room without encryption where Matrix puppets are invited (they join automatically) relays the message as a private post mentioning the users in the room (with the same conversation context as the first bridged message)
  - [x] Sending a private post mentioning an AP puppet makes the corresponding Matrix puppet create a private room without encryption with the users invited. If the private post is a reply to an already bridged private post, it is relayed as a message to the same room, inviting new users if necessary
- [x] ActivityPub outbox rooms
  - [x] Joining a Matrix room with an alias made from a puppet username joins or launches the creation of a public room corresponding to the AP user timeline
  - [x] Public posts from the AP user are relayed in the room
  - [x] Messages to the room from Matrix users are relayed as public posts mentioning the AP user
  - [x] Invite to room when mentioning on AP
- [x] Matrix outbox rooms
  - [x] Appservice bot accept invites to public rooms
  - [x] Relay events if they are created by the relevant user
  - [x] Relay events from other users as public posts mentioning the AP puppet
- [x] Bridging with Mobilizon
  - [x] Relay actor automatically accept follow and follows back
  - [x] Collection rooms are created when inviting an AP puppet
  - [x] Joining a collection room accepts the invitation
- [x] Object types
  - [x] Note objects are accepted
  - [x] Event objects (Mobilizon) are accepted
  - [x] Video objects (Peertube) are accepted
  - [ ] Article objects are accepted
- [x] Follows
  - [x] AP puppets automatically accept follows
  - [x] Matrix users can follow AP users by saying "!kazarma follow" in the AP outbox room
  - [x] Matrix users can unfollow AP users by saying "!kazarma unfollow" in the AP outbox room
- [x] Replies
  - [x] Matrix replies are bridged
  - [x] AP replies are bridged
- [x] Attachments
  - [x] Matrix attachments are bridged
  - [x] AP attachments are bridged
- [x] Formatting
  - [x] Matrix formatting is bridged
  - [x] AP formatting is bridged
- [x] Deletions
  - [x] Matrix deletions are bridged
  - [x] AP deletions are bridged
- [x] Mentions
  - [x] Matrix mentions are bridged
  - [x] AP mentions are bridged
- [ ] Likes
- [ ] Implement the AP S2S spec
  - [x] Actors
  - [x] Objects
  - [x] Inboxes
  - [ ] Collections
    - [ ] Outboxes
    - [ ] Followers
    - [ ] Followings
    - [ ] Liked
    - [ ] Likes
    - [ ] Shares
- [ ] Linking a Matrix account to a remote AP account
- [ ] Implement the AP C2S spec
