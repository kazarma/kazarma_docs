---
sidebar_position: 2
---

# Install using Docker Compose

## Installation

Download the necessary files:

```bash
wget -O docker-compose.yml https://gitlab.com/technostructures/kazarma/kazarma/-/raw/main/docker-compose.production.yml
wget -O .env https://gitlab.com/technostructures/kazarma/kazarma/-/raw/main/.env.example
```

## Configure

Modify the `.env` file according to your needs. The configuration options are explained [here](configuration).

Generate secrets:

```bash
(echo -n "DATABASE_PASSWORD="; openssl rand -hex 64) >> .env
(echo -n "SECRET_KEY_BASE="; openssl rand -hex 64) >> .env
(echo -n "HOMESERVER_TOKEN="; openssl rand -hex 64) | tee /dev/tty >> .env
(echo -n "ACCESS_TOKEN="; openssl rand -hex 64) | tee /dev/tty >> .env
```

## Configure your Matrix homeserver

[Configure you Matrix homeserver](homeserver-configuration), using the generated homeserver token and access token.

## Run the migrations

```bash
docker compose run --rm kazarma eval "Kazarma.Release.migrate()"
```

## Start Kazarma

```bash
docker compose up -d
```
