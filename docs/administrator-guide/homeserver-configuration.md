---
sidebar_position: 5
---

# Matrix homeserver configuration

Create a registration file for Kazarma:

```yaml
id: "Kazarma"
url: "https://kazarma.domain/matrix/"
as_token: "CHANGE_THIS"
hs_token: "CHANGE_THIS"
sender_localpart: "_kazarma"
namespaces:
  aliases:
    - exclusive: true
      regex: "#_ap_.+___.+"
  users:
    - exclusive: true
      regex: "@_ap_.+___.+"
```

- `url` points to your Kazarma instance, with a `/matrix/` route;
- `as_token` and `hs_token` should be generated as explained in the installation guides.
- `sender_localpart` is the username of the main bot;
- namespace regexes include the "puppet prefix" that is also part of Kazarma configuration. Here they also include the domain separator, just to be a little more restrictive.

Configure your Matrix homeserver to find it, for instance with Synapse:

```yaml
app_service_config_files:
  - kazarma.yaml
```
