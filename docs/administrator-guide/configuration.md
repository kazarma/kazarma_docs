---
sidebar_position: 4
---

# Configuration

kazarma is configured using environment variables. It will refuse to start if the required variables are not provided.

For optional environment variables, defaults are the recommended values to run kazarma beside your homeserver, bridging only your own users.

You may also want to [customize the frontend](#frontend-customization) or [enable observability features](#observability).

## Database configuration

### `DATABASE_HOST` **(required)**

The host to reach PostgreSQL.

### `DATABASE_USERNAME` **(required)**

The SQL username.

### `DATABASE_PASSWORD` **(required)**

The SQL password.

### `DATABASE_DB` **(required)**

The SQL database.

## Application service configuration

### `HOMESERVER_TOKEN` **(required)**

Token defined in the application service configuration file, will be used to authenticate the Matrix server against kazarma.

### `ACCESS_TOKEN` **(required)**

Token defined in the application service configuration file, will be used to authenticate kazarma against the Matrix server.

### `MATRIX_URL` **(required)**

URL to the Matrix server.

## Addresses

### `ACTIVITY_PUB_DOMAIN` **(required)**

ActivityPub domain for puppet actors. If different than the host, you need to serve a file at `domain/.well-known/host-meta`, containing a link to the real host, like [this](https://gitlab.com/technostructures/kazarma/kazarma/-/blob/f01918fab36102330a550ecd43e22ef55d5964e4/infra/dev/delegation/host-meta)

### `PUPPET_PREFIX`

**Default:** `_ap_`

Username prefix for Matrix puppet users that correspond to ActivityPub actors.

## General configuration

### `HOST` **(required)**

Host for the kazarma application, used to generate URLs.

### `SECRET_KEY_BASE` **(required)**

Phoenix's secret key base, used to sign session cookies. With Mix and Phoenix, it can be easily generated with `mix phx.gen.secret`.

### `BRIDGE_REMOTE`

**Default:** `false`

True or false, whether kazarma should bridge Matrix users from different homeservers (than the one beside kazarma), to the ActivityPub network.

### `HTML_SEARCH`

**Default:** `false`

True or false, whether to show the search field on kazarma HTML pages.

### `HTML_AP`

**Default:** `false`

True or false, whether to display profiles for ActivityPub actors. It can help Matrix users to get the (puppet) Matrix ID to reach an ActivityPub actor.

## Frontend customization

### `FRONTPAGE_HELP`

**Default:** `true`

True or false, whether the frontend should display integrated help.

### `FRONTPAGE_BEFORE_TEXT`

**Default:** `""`

Text to show before kazarma introduction on the index page.

### `FRONTPAGE_AFTER_TEXT`

**Default:** `""`

Text to show after kazarma introduction on the index page.

## Observability

### `SENTRY_DSN`

**Default:** ` `

Sentry DSN, if used.

### `RELEASE_LEVEL`

**Default:** `production`

Release level, used in Sentry.

### `ENABLE_PROM_EX`

**Default:** `false`

Enable Prometheus metrics.
