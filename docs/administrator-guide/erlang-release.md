---
sidebar_position: 1
---

# Install using an Erlang release

You can deploy Kazarma using plain Erlang releases. Those can be built and run on different systems, as long as they are similar (same architecture, distribution, ABI). For more information, see [the Elixir documentation](https://hexdocs.pm/mix/main/Mix.Tasks.Release.html#module-deployments).

## Build the release

```bash
git clone -b 1.0.0 --depth 1 https://gitlab.com/technostructures/kazarma/kazarma.git
cd kazarma
git submodule update --init --recursive
mix deps.get --only prod
MIX_ENV=prod mix release
```

The release is the directory `_build/prod/rel/kazarma`. Place it wherever you want, then cd to it.

## Configure

```bash
mv .env.example .env
```

Modify the `.env` file according to your needs. The configuration options are explained [here](configuration).

Generate secrets:

```bash
(echo -n "DATABASE_PASSWORD="; openssl rand -hex 64) >> .env
(echo -n "SECRET_KEY_BASE="; openssl rand -hex 64) >> .env
(echo -n "HOMESERVER_TOKEN="; openssl rand -hex 64) | tee /dev/tty >> .env
(echo -n "ACCESS_TOKEN="; openssl rand -hex 64) | tee /dev/tty >> .env
```

## Configure your Matrix homeserver

[Configure you Matrix homeserver](homeserver-configuration), using the generated homeserver token and access token.

## Run the migrations

From the release directory:

```bash
bin/kazarma eval "Kazarma.Release.migrate()"
```

## Run the release

```bash
bin/kazarma start
```
