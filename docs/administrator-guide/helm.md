---
sidebar_position: 3
---

# Install using Helm

Kazarma is published as a Helm chart. Instructions are available at [Artifact Hub](https://artifacthub.io/packages/helm/technostructures/kazarma).

[Kazarma Helm chart repository](https://gitlab.com/technostructures/kazarma/kazarma_helm)
